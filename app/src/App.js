import React, { useState } from "react";
import "./assets/css/App.css";
import Filter from "./components/Filter/filters";
import Content from "./components/Content/content";
import axios from "axios";

function App(props) {
  const [apiData, setApiData] = useState(null);
  const [apiError, setApiError] = useState("");
  const [historyUrl, setHistoryUrl] = useState("");

  const filterApiHandler = (filterType, value, history) => {
    let apiUrl = "https://api.spaceXdata.com/v3/launches?limit=100";
    let appendUrl = historyUrl;
    if (!appendUrl.includes(filterType)) {
      appendUrl += `&${filterType}=${value}`;
      setHistoryUrl(appendUrl);
    } else {
      let updatedUrlArr = appendUrl.split("&");
      let index = updatedUrlArr.findIndex((val) => {
        return val.includes(filterType);
      });
      updatedUrlArr.splice(index, 1, `${filterType}=${value}`);
      appendUrl = updatedUrlArr.reduce((output, currentElem, index) => {
        if (index !== 0) {
          output += `&${currentElem}`;
        } else {
          output += "";
        }
        return output;
      }, "");
      setHistoryUrl(appendUrl);
    }
    history.push(appendUrl);
    axios
      .get(apiUrl + appendUrl)
      .then((response) => setApiData(response.data))
      .catch((error) => setApiError(error));
  };
  return (
    <div className="App">
      <h1>Space X Launch Programs</h1>
      <div className="container">
        <div className="row">
          <span className="col-lg-2 col-sm-4 ">
            <Filter filterApiHandler={filterApiHandler} />
          </span>
          <span className="col-lg-10 col-sm-8 content-style">
            <Content apiData={apiData} apiError={apiError} />
          </span>
        </div>
      </div>
      <h4>Developed By: Shubham Arora</h4>
    </div>
  );
}

export default App;
