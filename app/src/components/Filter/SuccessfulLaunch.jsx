import React from 'react';
import '../../assets/css/Filter.css';
import {buttonHandler} from './filterButtonHandler'

const SuccessfulLaunchFilter =({filterApiHandler}) => {
    return <div className={'filter-margin'}>
        <h6 className={'filter-title'}>{'Successful Launch'}</h6>
        { buttonHandler('true', 'false',filterApiHandler, "launch_success")}
    </div>
}
export default SuccessfulLaunchFilter;