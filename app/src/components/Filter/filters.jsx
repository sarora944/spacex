import React from 'react';
import '../../assets/css/Filter.css';
import LaunchYearFilter from './LaunchYearFilter';
import SuccessfulLaunchFilter from './SuccessfulLaunch';
import SuccessfulLandingFilter from './SuccessfulLanding'

const Filter = ({filterApiHandler}) => {

    return (
        <div className={'filter-background'}>
            <h4 style={{ fontWeight: "bold", textAlign: "left" }}>Filters</h4>
            <LaunchYearFilter
                filterApiHandler={filterApiHandler} />
            <SuccessfulLaunchFilter
                filterApiHandler={filterApiHandler} />
            <SuccessfulLandingFilter
                filterApiHandler={filterApiHandler} />
        </div>
    );
}
export default Filter;