import React from 'react';
import { Route } from 'react-router-dom';

export const buttonHandler = (value1, value2, filterApiHandler, filterType) => {
    return <div className='row row-margin'>
        {value1 &&
            <Route render={({ history }) => {
                return <button type="button"
                    className="btn btn-success right-button-margin button-style"
                    onClick={() => filterApiHandler(filterType, value1, history)}
                >
                    {value1}
                </button>
            }} />
        }
        {value2 &&
            <Route render={({ history }) => {
                return <button
                    type="button"
                    className="btn btn-success btn-md button-style"
                    onClick={() => filterApiHandler(filterType, value2, history)}
                >
                    {value2}
                </button>
            }} />
        }
    </div>

}