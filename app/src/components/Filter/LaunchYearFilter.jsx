import React from 'react';
import '../../assets/css/Filter.css';
import  {buttonHandler} from './filterButtonHandler';

const LaunchYearFilter =({filterApiHandler}) => {
    return <div className={'filter-margin'}>
        <h6 className={'filter-title'}>{'Launch Year'}</h6>
        { buttonHandler(2006, 2007, filterApiHandler, "launch_year" )}
        { buttonHandler(2008, 2009, filterApiHandler, "launch_year")}
        { buttonHandler(2010, 2011, filterApiHandler, "launch_year")}
        { buttonHandler(2012, 2013, filterApiHandler, "launch_year")}
        { buttonHandler(2014, 2015, filterApiHandler, "launch_year")}
        { buttonHandler(2016, 2017, filterApiHandler, "launch_year")}
        { buttonHandler(2018, 2019, filterApiHandler, "launch_year")}
        { buttonHandler(2020,'', filterApiHandler, "launch_year")}
    </div>
}
export default LaunchYearFilter;