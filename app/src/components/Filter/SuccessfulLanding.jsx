import React from 'react';
import '../../assets/css/Filter.css';
import {buttonHandler} from './filterButtonHandler'

const SuccessfulLandingFilter =({filterApiHandler}) => {
    return <div className={'filter-margin'}>
        <h6 className={'filter-title'}>{'Successful Landing'}</h6>
        { buttonHandler('true', 'false',filterApiHandler, "land_success")}
    </div>
}
export default SuccessfulLandingFilter;