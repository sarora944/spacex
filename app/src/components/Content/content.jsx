import React, { useEffect, useState } from 'react';
import axios from 'axios';
import '../../assets/css/content.css';

const Content = (props) => {
    const [data, setData] = useState([]);
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(true);
    const [limit, setLimit] = useState(10)
    useEffect(() => {
        if (!props.apiData) {
            axios.get(`https://api.spaceXdata.com/v3/launches?limit=${limit}`)
                .then(response => {
                    setData(response.data)
                    setLoading(false)
                })
                .catch(error => {
                    setLoading(false)
                    setError(error)
                })
        }
        else if (props.apiData) {
            setData(props.apiData)
        }
        else if (props.apiError) {
            setError(props.apiError)
        }
    }, [props.apiData, props.apiError, limit])

    const displayContent = obj => {
        return <div className="col-md-6 col-lg-3 main-container" key={obj.flight_number}>
            <div className={'image-background'}>
                <img className={'image-style'}
                    src={obj.links.mission_patch_small}
                    alt={obj.mission_name} />
            </div>
            <h6 className={'font-color'}>
                {`${obj.mission_name} #${obj.flight_number}`}
            </h6>
            <div>
                <span className={'title'}>
                    {'Mission Id: '}
                </span>
                <span>
                    {obj.mission_id.length > 0 ?
                        obj.mission_id.map((id) => {
                            return <li key={id} className={'font-color'}>{id}</li>
                        }) :
                        '-'
                    }
                </span>
            </div>
            <div>
                <span className={'title'}>
                    {'Launch Year: '}
                </span>
                <span className={'font-color'}>
                    {obj.launch_year}
                </span>
            </div>
            <div>
                <span className={'title'}>
                    {'Successful Launch: '}
                </span>
                <span className={'font-color'}>
                    {obj.launch_success ? "True" : "False"}
                </span>
            </div>
            <div>
                <span className={'title'}>
                    {'Successful Land: '}
                </span>
                <span className={'font-color'}>
                    {obj?.rocket?.first_stage?.cores?.map(obj => {
                        return obj.land_success ? "True" : "False"
                    })}
                </span>
            </div>
        </div>
    }
    const incrementLimit = () => {
        setLimit(limit + 10)
    }
    return (
        <div className={'content-background'}>
            {data.length > 0 && !error && !loading ?
                <div className="container">
                    <div className="row" >
                        {
                            data.map(obj => {
                                return displayContent(obj)
                            })
                        }
                    </div>
                </div> : loading ?
                    <h4>{'Loading...'}</h4> : null
            }
            {data.length === 0 && !loading &&
                <h3>No Results Found</h3>}
            {error &&
                <div>
                    {"Error retriving the data"}
                </div>
            }
            {
                !error && data.length !== 0 &&
                limit < 100 && !props?.apiData &&
                <button
                    type="button"
                    style={{ cursor: "pointer" }}
                    onClick={incrementLimit}
                    class="btn btn-outline-success">
                    {'Show More'}
                </button>
            }
        </div>
    )
}

export default Content;